/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hcos.assignment1_tinocojaime;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author jtinocotejeida
 */
public class OJTReflection {
    private static int maxStudentID;
    private int studentId;
    private String firstName;
    private String lastName;
    private String reflectionText;

    public OJTReflection() {
        //do nothing other than set the registration Id.
        this.studentId = ++maxStudentID;
    }

    /**
     * Default constructor which will get info from user
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public OJTReflection(boolean getFromUser) {
        if (getFromUser) {
            System.out.println("Enter first name");
            this.firstName = Utility.getInput().nextLine();

            System.out.println("Enter last name");
            this.lastName = Utility.getInput().nextLine();

            System.out.println("Enter reflection Text");
            this.reflectionText = Utility.getInput().nextLine();

        }

        //Set the registration id
        this.studentId = ++maxStudentID;
    }

    /**
     * Custom constructor with all info
     *
     * @param studentId
     * @param firstName
     * @param lastName
     * @param dob
     *
     * @author BJ MacLean
     * @since 20150917
     */
    public OJTReflection(int studentId, String firstName, String lastName, String reflectionText) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.reflectionText = reflectionText;
    }

    /**
     * constructor which will create from String array
     *
     */
    public OJTReflection(String[] parts) {
        this(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
        /*
         This makes sure that we capture/set the maximum registration id as we load
         all of the entries from the file.  Then when we add a new camper it will
         use this to set the next registration id.
         */
        if (Integer.parseInt(parts[0]) > maxStudentID) {
            maxStudentID = Integer.parseInt(parts[0]);
        }
    }

    
    
    /**
     * constructor which will create from String array
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public String getJson()
    {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    /**
     * give back a new instance based on the json string passed in.
     *
     * @since 20190918
     * @author BJ MacLean
     */
    public static OJTReflection newInstance(String jsonIn) {
        Gson gson = new Gson();
        return gson.fromJson(jsonIn, OJTReflection.class);
    }

    public static int getMaxStudentID() {
        return maxStudentID;
    }

    public static void setMaxStudentID(int maxStudentID) {
        OJTReflection.maxStudentID = maxStudentID;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getReflectionText() {
        return reflectionText;
    }

    public void setReflectionText(String reflectionText) {
        this.reflectionText = reflectionText;
    }




    

    @Override
    public String toString() {
        return "Student ID= " + studentId + ", first Name= " + firstName + ", last Name= " + lastName + ", Reflection Text= " + reflectionText;
    }

    
}
