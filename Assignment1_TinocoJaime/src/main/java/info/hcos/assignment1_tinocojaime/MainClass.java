/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hcos.assignment1_tinocojaime;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author jtinocotejeida
 */
public class MainClass {
    public static String MENU = "Menu Options:\nA) Add/Update Student\nS) Show Student\nX) Exit";
    public static final String PATH = "/cis2232/";
    public static String FILE_NAME = PATH + "reflection.json";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        //Create a file
        Files.createDirectories(Paths.get(PATH));

        ArrayList<OJTReflection> theList = new ArrayList();
        loadStudents(theList);
        String option;
        do {
            System.out.println(MENU);
            option = Utility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    OJTReflection newStudent = new OJTReflection(true);
                    theList.add(newStudent);

                    //Also write to the file when a new camper was added!!
                    BufferedWriter bw = null;
                    FileWriter fw = null;

                    try {
                        fw = new FileWriter(FILE_NAME, true);
                        bw = new BufferedWriter(fw);

                        String jsonString = newStudent.getJson();
                        //bjm 20190917 Adding line break
                        bw.write(jsonString+System.lineSeparator());
                       
                        System.out.println("Done");
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (bw != null) {
                                bw.close();
                            }

                            if (fw != null) {
                                fw.close();
                            }

                        } catch (IOException ex) {

                            ex.printStackTrace();

                        }

                    }

                    break;
                case "S":
                    System.out.println("Here are the Students");
                    for (OJTReflection student : theList) {
                        System.out.println(student);
                    }
                    break;
                case "U":
                    System.out.println("which Student would you like to update");
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }
    
    public static void loadStudents(ArrayList ojtReflection) {
        System.out.println("Loading Students from file");
        int count = 0;

        try {
            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));

            for (String current : test) {
                System.out.println("Loading:  " + current);
                //Get a camper from the string
                //Camper temp = new Camper(current);
                OJTReflection temp = OJTReflection.newInstance(current);
                ojtReflection.add(temp);
                count++;
            }

        } catch (IOException ex) {
            System.out.println("Error loading students from file.");
            System.out.println(ex.getMessage());
        }

        System.out.println("Finished...Loading students from file (Loaded " + count + " OJTreflection)\n\n");

    }
    
    
}
